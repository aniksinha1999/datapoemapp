import axios from "axios";
import React from "react";
import {Card} from "@mui/material";

import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import FileUpload from "./FileUpload";
export default class  ProductList extends React.Component {
    state = {
        productlist: []
    }

    componentDidMount() {
        axios.get('http://localhost:8080/ecomm/productlist').then(
            (response) => {
                this.setState({productlist: response.data.response})
                console.log(response)
            }
        ).catch();
    }
    buyProduct()
    {

    }

    render() {
        return (
            <div className="container-fluid">
                <FileUpload></FileUpload>
                <hr></hr>
                <div className="row">
                    <div className="col-lg-12">
                    <table className="table table-bordered">
                        <thead>
                        <tr>

                            <th>Product Display</th>
                            <th>Name</th>
                            <th>Price</th>

                            <th>Category</th>
                            <th>Rating </th>
                            <th>Stock</th>

                        </tr>

                        </thead>
                        <tbody>
                        {
                            this.state.productlist.map((app) =><tr key={app.id}><td><img class="rounded" id="imgid"src={app.image}/> </td>
                                    <td>{app.title}</td>

                                    <td>{app.price}</td>
                                    <td>{app.category}</td>
                                    <td>{app.rating}</td>
                                <td>{app.count}</td>


                                </tr>




                            )

                        }

                        </tbody>
                    </table>
                    </div>
                </div>
            </div>




        )
    }






    }
