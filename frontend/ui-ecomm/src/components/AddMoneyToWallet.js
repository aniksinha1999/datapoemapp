import axios from "axios";
import React from "react";
import Header from "./Header";
export default class AddMoneyToWallet extends React.Component
{
state={email:'',wallet_balance:'',message:[],moneysubmitted:false}
    onChangeEmail=(e)=>
    {
        this.setState({email:e.target.value})
    }
    OnChangeBalance=(e)=>
    {
        this.setState({wallet_balance:e.target.value})
    }
    onTransaction=(e)=>
    {
        e.preventDefault()
        axios.post(`http://localhost:8080/ecomm/updatewallet/${this.state.email}/${this.state.wallet_balance}`,this.state).then(
            (response)=>
            {
                if(response.data.result==="done")
                {
                    this.setState({moneysubmitted:true})
                    this.setState({message:"Money added"})

                }
            }
        )
    }
    render()
    {
        const{email,wallet_balance}=this.state
        return(
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-12">
                        <Header></Header>
                    </div>


                    </div>


                <br></br>
                <br></br>

                <div className="card">
                    <div className="card-body">
                        <h4>Add money to your wallet! </h4>

                        <div className="row">
                            <div className="col-lg-4">
                                <input type="text" className="form-control" onChange={this.onChangeEmail} value={email} required autoFocus/>
                                <small>Enter Email</small>
                            </div>
                            <div className="col-lg-4">
                                <input type="number" className="form-control" onChange={this.OnChangeBalance}
                                       value={wallet_balance}/>
                                <small>Enter amount</small>
                            </div>
                        </div>
                        <br></br>
                        <br></br>

                        <div className="row">
                            <div className="col-lg-10">
                                <button className="btn btn-success" onClick={this.onTransaction}>Submit</button>
                            </div>
                        </div>
                        <br></br>
                        <br></br>

                        <div className="row">
                            <div className="col-lg-8">
                                { this.state.moneysubmitted&&(
                                    <p className="alert alert-success">
                                        {this.state.message}
                                    </p>)
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}