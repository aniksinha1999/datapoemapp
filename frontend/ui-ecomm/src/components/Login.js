import React from "react";
import axios from "axios";
import Header from "./Header";
import ProductList from "./ProductList";
export  default class  Login extends  React.Component {
    state = {
        email: '',
        password: '',
        productlist: [],

        logindone:false,
        loginfail:false,
        message:[],
        profile:[],
        submitmessage:[]
    }
    onChanger = (e) => {
        this.setState({email: e.target.value})

    }
    Onchangepass = (e) =>
    {
        this.setState({password:e.target.value})
    }
    componentDidMount() {
        axios.get('http://localhost:8080/ecomm/productlist').then(
            (response) => {
                this.setState({productlist: response.data.response})
                console.log(response)
            }
        ).catch();
    }
onlogin=(e)=> {
    e.preventDefault()
    axios.get(`http://localhost:8080/ecomm/login/${this.state.email}/${this.state.password}`, this.state).then(
        (response) => {
            if (response.data.result === null) {
                this.setState({loginfail:true})
                this.set({message:"Invalid Credentials!Please check or register"})

            }
            else {
                this.setState({logindone:true})
                this.setState({profile:response.data.result})
            }

        })
}


render()
{ const {email,password,profile}=this.state
    return(
        <form onSubmit={this.onlogin}>
        <div className="container-fluid">
            <div className="row">
                <div className="col-lg-12">

                    <Header></Header>
                </div>


            </div>

            <br></br>
            <h5>Login here !</h5>
            <div className="row">

                <div className="col-lg-3">


                    <input type="text"  name="email" value={email} onChange={this.onChanger} className="form-control"/>
                    <small>Enter email</small>


                </div>
                <div className="col-lg-3">

                    <input type="password" name="password" value={password} onChange={this.Onchangepass } className="form-control"/>
                    <small>Enter password</small>

                </div>
                <div className="col-lg-3">
                    <button className="btn btn-primary" id="login" >Submit</button>
                </div>
            </div>
            <hr></hr>
            <br></br>
            {this.state.loginfail&&(<p className="alert alert-danger">Invalid credentials !Please register here or check</p>)}


            {this.state.logindone &&(
                <div className="card">
                    <div className="card-body">
                        <h3>Your Profile</h3>
                        <label>Name</label>:{profile.name}<br></br>
                        <label>Email</label>:{profile.email}<br></br>
                        <label>Wallet Balance</label>:{profile.wallet_balance}
                        <br></br>
                        <a href="/wallet" className="btn btn-primary btn-sm" >Add money</a>
                        <hr></hr>
                        <div className="row">
                            <div className="col-lg-12">
                                <p className="alert alert-secondary">
                                    {
                                        this.state.submitmessage
                                    }
                                </p>
                            </div>
                        </div>
                        <hr></hr>
                        <div className="row">
                            <div className="col-lg-12">
                                <table className="table table-bordered">
                                    <thead>
                                    <tr>

                                        <th>Product Display</th>
                                        <th>Name</th>
                                        <th>Price</th>

                                        <th>Category</th>
                                        <th>Rating</th>
                                        <th>Stock</th>

                                    </tr>

                                    </thead>
                                    <tbody>
                                    {
                                        this.state.productlist.map((app) => <tr key={app.id}>
                                                <td><img className="rounded" id="imgid" src={app.image}/></td>
                                                <td>{app.title}</td>

                                                <td>{app.price}</td>
                                                <td>{app.category}</td>
                                                <td>{app.rating}</td>
                                                <td>{app.count}</td>
                                                <td><a href={'http://localhost:8080/ecomm/buy/'+ profile.id+'/' +app.id} type="button" className="btn btn-primary">Buy
                                                    now</a></td>


                                            </tr>
                                        )

                                    }

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>)
            }

        </div>
        </form>


    )
}
}