import React from "react";
import axios from "axios";
import Link from 'react';
import Header from "./Header";

export default class Registration extends React.Component {
    state = {
        name: '',
        email: '',
        password: '',

        submit: false,
        message: []
    }
    onChangeHandler = (e) => {
        this.setState({[e.target.name]: e.target.value})
        console.log(e.target.value)
    }
    onSubmitForm = (e) => {
        e.preventDefault()
        console.log(this.state)
        axios.post('http://localhost:8080/ecomm/register', this.state).then(
            (response) => {
                if (response.data.result === "done") {
                    this.setState({submit: true})
                    this.setState({message: "Registration Successfull"})

                }
            }
        )
    }

    render() {
        const {name, email, password} = this.state
        return (

            <form onSubmit={this.onSubmitForm}>
                <div className="row">
                    <div className="col-lg-12">
                        <Header></Header>
                    </div>
                </div>

                <div className="card">
                    <div className="card-body">


                        <div className="container-fluid">


                            <div className="row">

                                <div className="col-lg-9">
                                    <h1>Register here!</h1>
                                </div>
                            </div>


                            <div className="row">

                                <div className="col-lg-6 ">
                                    <small>Enter your name</small>
                                    <input type="text" className="form-control" value={name} name="name"
                                           onChange={this.onChangeHandler}/>
                                </div>
                            </div>
                            <br></br>
                            <div className="row">

                                <div className="col-lg-6">
                                    <small>Enter email</small>
                                    <input type="text" className="form-control" value={email} name="email"
                                           onChange={this.onChangeHandler}/>
                                </div>
                            </div>
                            <br></br>
                            <div className="row">

                                <div className="col-lg-6">
                                    <small>Enter password</small>
                                    <input type="password" className="form-control" value={password} name="password"
                                           onChange={this.onChangeHandler}/>
                                </div>
                            </div>
                            <br></br>

                            <br></br>
                            <div className="row">


                                <div className="col-lg-9">
                                    <button className="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            <hr></hr>
                            <div className="row">

                                <div className="col-lg-9">
                                    <a href="/login"><small>Already have an account?Login here</small></a></div>
                            </div>
                            <br></br>
                            <div className="row">
                                <div className="col-lg-8">
                                    {
                                        this.state.submit && (

                                            <p className="alert alert-success">{this.state.message}</p>
                                        )
                                    }
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </form>


        )
    }

}