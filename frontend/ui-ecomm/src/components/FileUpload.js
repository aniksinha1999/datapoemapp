import axios from 'axios';

import React,{Component} from 'react';

 export default   class FileUpload extends Component {

    state = {

        // Initially, no file is selected
        selectedFile: null
    };

    // On file select (from the pop up)
    onFileChange = event => {

        // Update the state
        this.setState({ selectedFile: event.target.files[0] });

    };

    // On file upload (click the upload button)
    onFileUpload = () => {

        // Create an object of formData
        const formData = new FormData();

        // Update the formData object
        formData.append(
            "myFile",
            this.state.selectedFile,
            this.state.selectedFile.name
        );

        // Details of the uploaded file
        console.log(this.state.selectedFile);

        // Request made to the backend api
        // Send formData object
        axios.post("http://localhost:8080/ecomm/upload", formData);
    };

    // File content to be displayed after
    // file upload is complete
    fileData = () => {

        if (this.state.selectedFile) {

            return (




                <div>
                    <br />
                    <h4>Choose before Pressing the Upload button</h4>
                </div>
            );
        }
    };

    render() {

        return (
            <div className="container-fluid">
                <br></br>
                <br></br>
                <h5>Upload your file here!</h5>
                <div className="row">

                    <div className="col-lg-6">
                        <input  className="form-control-file border" type="file" onChange={this.onFileChange} />
                    </div>
                    <div className="col-lg-2">

                        <button onClick={this.onFileUpload} className="btn btn-primary btn-sm">
                            Upload!
                        </button>
                    </div>

                </div>


                <div>

                </div>
                {this.fileData()}
            </div>
        );
    }
}


