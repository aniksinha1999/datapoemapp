import ProductList from "./ProductList";
import React from "react";
import Header from "./Header";

export  default class  Admin extends  React.Component
{
    render()
    {
        return(
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <Header></Header>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <ProductList></ProductList>
                    </div>
                </div>

            </div>

        )
    }
}