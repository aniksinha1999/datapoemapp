import logo from './logo.svg';
import './App.css';
import ProductList from "./components/ProductList";
import Registration from "./components/Registration";
import Login from "./components/Login";
import AddMoneyToWallet from "./components/AddMoneyToWallet";
import {BrowserRouter ,  Route, Router, Routes,Switch} from "react-router-dom";
import Admin from "./components/Admin";
import HomePage from "./mergedcomponents/HomePage";

function App() {
  return(
<BrowserRouter>

    <Route  exact path="/" component={HomePage}/>
  <Route path="/login" component={Login}/>
  <Route path="/admin" component={Admin}/>
  <Route path="/wallet" component={AddMoneyToWallet}/>





</BrowserRouter>
  )
}

export default App;
