package com.batch.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.batch.helper.FileUploadHelper;
import com.batch.model.FileUpload;
import com.batch.repository.FileUploadRepo;

@Service
public class FileUploadService {
	@Autowired
	FileUploadRepo fileuploadrepo;
	public void save (MultipartFile file)
	{
		 try {
		      List<FileUpload> files = FileUploadHelper.excelToTutorials(((MultipartFile) file).getInputStream());
		      fileuploadrepo.saveAll(files);
		    } catch (IOException e) {
		      throw new RuntimeException("fail to store excel data: " + e.getMessage());
		    }
	}
	

}
