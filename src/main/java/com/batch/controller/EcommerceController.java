package com.batch.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.batch.model.Productinfo;
import com.batch.model.UserRegistration;
import com.batch.repository.Productinforepo;
import com.batch.repository.UserRegistrationRepo;



@RestController
public class EcommerceController {
@Autowired
Productinforepo productinfo;
@Autowired
UserRegistrationRepo userregrepo;
	@GetMapping(value="ecomm/productlist")
	public Map<String ,List<Productinfo>> getallproducts()
	{
		Map<String ,List<Productinfo>>output=new HashMap<String ,List<Productinfo>>();
		output.put("response", (List<Productinfo>) productinfo.findAll());
		return output;
	}
	@GetMapping(value="ecomm/buy/{pid}/{id}")
	@CrossOrigin
	
	public Map<String ,String>buyproducts(@PathVariable("id")int id,@PathVariable("pid")int pid)
	
	{
		Map<String,String>out=new HashMap<String,String>();
		Productinfo product=productinfo.findById(id).get();
		UserRegistration userregistration=userregrepo.findById(pid).get();
		product.setCount(product.getCount()-1);
		userregistration.setWallet_balance(userregistration.getWallet_balance()-product.getPrice());
		userregrepo.save(userregistration);
	
		productinfo.save(product);
		
		out.put("result", "Purchased item is"+product.getTitle()+" "+"Avaliable wallet balance"+userregistration.getWallet_balance()+""+"Please go back and login again to see the upated value");
		return out;
		
		
	}

}
