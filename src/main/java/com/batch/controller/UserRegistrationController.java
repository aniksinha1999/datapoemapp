package com.batch.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.batch.model.UserRegistration;

import com.batch.repository.UserRegistrationRepo;

@RestController
public class UserRegistrationController {
	@Autowired
	UserRegistrationRepo useregistrationrepo;
	@PostMapping(value="/ecomm/register")
	@CrossOrigin
	public Map<String ,String > submit( @RequestBody UserRegistration userregistration )
	{
		Map<String,String>output=new HashMap<String ,String >();
		userregistration.setWallet_balance(0);
		
		
	
	
		useregistrationrepo.save(userregistration);
		output.put("result","done");
		return output;
	}
	@GetMapping(value="/ecomm/login/{email}/{password}")
	@CrossOrigin
	public Map<String,UserRegistration>getdata(@PathVariable("email")String email,@PathVariable("password")String password)
	{
		Map<String,UserRegistration>output=new HashMap<String ,UserRegistration>();
		UserRegistration userregistation=new UserRegistration();
		 userregistation=useregistrationrepo.findByEmailAddress(email, password);
		 
		 output.put("result", userregistation);
		 return output;
		
	}
	@PostMapping(value="/ecomm/updatewallet/{email}/{amount}")
	@CrossOrigin
	public Map<String,String >addwallet(@PathVariable("email")String email,@PathVariable("amount")double amount)
	{
		Map<String ,String>output=new HashMap<String,String>();
		UserRegistration userregistation=new UserRegistration();
		amount=userregistation.getWallet_balance()+amount;
		
		useregistrationrepo.updateWalletbalance(amount, email);
		
		output.put("result","done");
		
		
		
		return output;
	}
	
	

}
