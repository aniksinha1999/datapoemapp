package com.batch.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import com.batch.model.FileUpload;

public class FileUploadHelper {
  public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
  static String[] HEADERs = {  "id"	,"title",	"price"	,"description",	"category",	"image","rating",	"count" };
  static String SHEET = "Product";
  public static boolean hasExcelFormat(MultipartFile file) {
    if (!TYPE.equals(file.getContentType())) {
      return false;
    }
    return true;
  }
  public static List<FileUpload> excelToTutorials(InputStream is) {
    try {
      Workbook workbook = new XSSFWorkbook(is);
      Sheet sheet = workbook.getSheet(SHEET);
      Iterator<Row> rows = sheet.iterator();
      List<FileUpload> file = new ArrayList<FileUpload>();
      int rowNumber = 0;
      while (rows.hasNext()) {
        Row currentRow = rows.next();
        // skip header
        if (rowNumber == 0) {
          rowNumber++;
          continue;
        }
        Iterator<Cell> cellsInRow = currentRow.iterator();
        FileUpload fileupload = new FileUpload();
        int cellIdx = 0;
        while (cellsInRow.hasNext()) {
          Cell currentCell = cellsInRow.next();
          switch (cellIdx) {
          case 0:
            fileupload.setId((int) currentCell.getNumericCellValue());
            break;
          case 1:
            fileupload.setTitle(currentCell.getStringCellValue());
            break;
          case 2:
          fileupload.setPrice(currentCell.getNumericCellValue());
            break;
          case 3:
            fileupload.setDescription(currentCell.getStringCellValue());
            break;
          case 4:
        	  fileupload.setCategory(currentCell.getStringCellValue());
        	  break;
          case 5:
        	  fileupload.setImage(currentCell.getStringCellValue());
        	  break;
          case 6:
        	  fileupload.setRating(currentCell.getNumericCellValue());
        	  break;
          case 7:
        	  fileupload.setCount(currentCell.getNumericCellValue());
        	  
          default:
            break;
          }
          cellIdx++;
        }
        file.add(fileupload);
      }
      workbook.close();
      return file;
    } catch (IOException e) {
      throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
    }
  }
}
