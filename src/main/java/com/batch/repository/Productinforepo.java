package com.batch.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.batch.model.Productinfo;
@Repository
public interface Productinforepo extends CrudRepository<Productinfo, Integer> {

}
