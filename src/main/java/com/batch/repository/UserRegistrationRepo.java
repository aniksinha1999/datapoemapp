package com.batch.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.batch.model.UserRegistration;

public interface UserRegistrationRepo  extends CrudRepository<UserRegistration,Integer>{

	 @Query(value = "SELECT * FROM userregistration WHERE email = ?1 and password=?2", nativeQuery = true)
	    UserRegistration findByEmailAddress(@Param("email")String email,@Param("password")String password);
	 @Query(value="update userregistration set wallet_balance=?1  where email=?2" ,nativeQuery=true)
	 @Modifying
	 @Transactional
	void updateWalletbalance(@Param("wallet_balance")double amount,@Param("email")String email);
	 
}
