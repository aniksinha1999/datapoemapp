package com.batch.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.batch.model.FileUpload;
@Repository
public interface FileUploadRepo extends CrudRepository<FileUpload, Integer> {

}
